<?php
include 'app/core/app.php';
if(isset($_POST['author'])) {setcookie('author', $_POST['author'], time() + 365*24*3600, null, null, false, true);}
include 'app/tpl/header.php';
$cookie = (isset($_COOKIE['author'])) ? $_COOKIE['author'] : 'Anonymous';
if(isset($_GET['a'])) {
	switch($_GET['a']) {
		case 'offline':
			$result = (isset($_GET['vote'])) ? $_GET['vote'] : array('AGREE'=>0, 'ABSTAIN'=>0, 'WAITING'=>0, 'INDIFFERENT' => 0, 'DISAGREE'=>0, 'BLOCK'=>0, 'TOTAL'=>0);
			include 'app/tpl/offline.form.php';
			if(isset($_GET['vote'])) {
				$result['TOTAL'] = $result['AGREE'] + $result['ABSTAIN'] + $result['DISAGREE'] + $result['WAITING'] +  $result['BLOCK'] + $result['QUARANTENEUFTROIS'];
				if(voting($result)) {infobox('&#10003; La proposition a été adoptée.', 'result');} else {infobox('&#10060; La proposition a été rejetée.', 'error');}
				include 'app/tpl/result.php';
			}
		break;
		case 'help':
			include 'app/tpl/help.php';
		break;
		case 'sandbox':
		/* dev zone */
		// placeholder_content();
		break;
		case 'angerona':
			$query = $bdd->query('SELECT e.id_proposal, e.email, e.token, p.content, p.id, p.deadline  FROM email e, proposal p WHERE e.id_proposal = p.id ORDER BY deadline DESC');
			echo '<ul>';
			while($data = $query->fetch()) {
				echo '<li><a href="'.ROOT.'?id='.$data['id_proposal'].'&token='.$data['token'].'">#'.$data['id_proposal'].' '.$data['email'].'</a></li>';
			}
			echo '</ul>';
		break;
		case 'list':
			if(isset($_GET['sort'])) {
				switch($_GET['sort']) {
					case 'hot' :
						$query = $bdd->query('SELECT p.id, p.deadline, p.content, COUNT(id_proposal) as hot FROM proposal p, debate d WHERE d.id_proposal = p.id GROUP BY id_proposal ORDER BY hot DESC');
					break;
					case 'participate':
						$query = $bdd->prepare('SELECT e.id_proposal as id, e.email, p.deadline, p.content, p.id FROM email e, proposal p WHERE p.id = e.id_proposal AND e.email=? ORDER BY e.id_proposal DESC');
						$query->execute(array(urldecode(is_email($_GET['email']))));
					break;
				}
			}
			else {
				$query = $bdd->query('SELECT * FROM proposal ORDER BY time DESC');
			}
			echo '<a href="?a=list">Les plus récentes</a> 
			<a href="?a=list&sort=hot">Les plus débattus</a>';
			include 'app/tpl/participate.form.php';
			echo '<ul>';
			while($data = $query->fetch()) {
				$status = (time() >= $data['deadline']) ? '[FINI] ' : '[EN COURS] ';
				echo '<li><a href="index.php?id='.$data['id'].'">'.$status.truncature($data['content']).'</a></li>';
			}
			echo '</ul>';
		break;
	}
}
else {
	if(isset($_GET['id'])) {
		$query = $bdd->prepare('SELECT time, deadline, content, id, open FROM proposal WHERE id=:id');
		$query->execute(array('id'=>$_GET['id']));
		$data = $query->fetch();
		$deadline = $data['deadline'];
		$is_open = $data['open'];
		include 'app/tpl/proposal.php';
		$_GET['token'] = isset($_GET['token']) ? $_GET['token'] : null;
		$query = $bdd->prepare('SELECT d.id, d.time, d.author, d.status, d.argument, d.token, e.email FROM debate d, email e WHERE d.id_proposal=:id AND d.token=e.token ORDER BY time ASC');
		$query->execute(array('id'=>$_GET['id']));
		$result = array('AGREE'=>0, 'ABSTAIN'=>0, 'WAITING'=>0, 'INDIFFERENT' => 0, 'DISAGREE'=>0, 'BLOCK'=>0, 'QUARANTENEUFTROIS'=>0, 'TOTAL'=>0);
		while($data = $query->fetch()) {
			$delete = ($data['token'] == $_GET['token']) ? '<a href="'.ROOT.'?id='.$_GET['id'].'&token='.$_GET['token'].'&delete='.$data['id'].'">&#10060;</a>' : '';
			$shoutat = ($data['token'] != $_GET['token'] AND isset($_GET['token'])) ? '<a href="javascript:reply(\'@['.$data['author'].'](#'.$data['id'].') : \');">@</a>' : '';
			$anchor = '<a href="#'.$data['id'].'">#</a>';
			$author = '<span rel="author" title="'.obfuscation_email($data['email']).'">'.$data['author'].'</span>';
			$avatar = '<img src="'.avatar($data['email']).'" alt="avatar" class="avatar"/>';
			include 'app/tpl/comment.php';
			$result['AGREE'] = ($data['status'] == 'AGREE') ? $result['AGREE']+1  : $result['AGREE'];
			$result['ABSTAIN'] = ($data['status'] == 'ABSTAIN') ? $result['ABSTAIN']+1  : $result['ABSTAIN'];
			$result['WAITING'] = ($data['status'] == 'WAITING') ? $result['WAITING']+1  : $result['WAITING'];
			$result['INDIFFERENT'] = ($data['status'] == 'INDIFFERENT') ? $result['INDIFFERENT']+1  : $result['INDIFFERENT'];
			$result['DISAGREE'] = ($data['status'] == 'DISAGREE') ? $result['DISAGREE']+1  : $result['DISAGREE'];
			$result['BLOCK'] = ($data['status'] == 'BLOCK') ? $result['BLOCK']+1  : $result['BLOCK'];
			$result['QUARANTENEUFTROIS'] = ($data['status'] == 'QUARANTENEUFTROIS') ? $result['QUARANTENEUFTROIS']+1  : $result['QUARANTENEUFTROIS'];
			$result['TOTAL'] =  (in_array($data['status'], $status_type['poll'])) ? $result['TOTAL']+1 : $result['TOTAL'];
		}
		$query = $bdd->prepare('SELECT count(id_proposal) as total, SUM(poll) as voting FROM email WHERE id_proposal=?  GROUP BY id_proposal');
		$query->execute(array($_GET['id']));
		$data = $query->fetch();
		include 'app/tpl/progress.php';
		include 'app/tpl/result.php';
		if(time() >= $deadline) {
			if(voting($result)) {infobox('&#10003; La proposition a été adoptée.', 'result');} else {infobox('&#10060; La proposition a été rejetée.', 'error');}
			infobox('Désolé, la consultation est expirée !', 'error');
		}
		else {
			$_GET['token'] = isset($_GET['token']) ? $_GET['token'] : '';
			$query = $bdd->prepare('SELECT token, poll FROM email WHERE token=? AND id_proposal=?');
			$query->execute(array($_GET['token'], $_GET['id']));
			$data = $query->fetch();
			if($_GET['token'] == $data['token'] and isset($data) AND $data['token'] !='') {
				if(isset($_POST['argument'])) {
					if($data['poll'] == 0 AND in_array($_POST['status'], $status_type['poll'])) {
						sql('INSERT INTO `debate`(`id_proposal`,`time`,`author`, `status`, `argument`, `token`) VALUES (?, ?, ?, ?, ?,?)', array($_GET['id'], time(), $_POST['author'], $_POST['status'], $_POST['argument'], $data['token']));
						sql('UPDATE `email` SET poll = 1 WHERE token=? AND id_proposal=?', array($_GET['token'], $_GET['id']));
						redirect(ROOT.'?id='.$_GET['id'].'&token='.$_GET['token'].'&r='.time());
					}
					else {
						infobox('Vous (ou quelqu’un d’autre) avez déjà voté !', 'neutral');
					}
					if(in_array($_POST['status'], $status_type['reaction']) AND isset($_POST['argument'])) {
						sql('INSERT INTO `debate`(`id_proposal`,`time`,`author`, `status`, `argument`, `token`) VALUES (?, ?, ?, ?, ?,?)', array($_GET['id'], time(), $_POST['author'], $_POST['status'], $_POST['argument'], $data['token']));
						redirect(ROOT.'?id='.$_GET['id'].'&token='.$_GET['token'].'&r='.time());
					}
				}
				if(isset($_GET['delete'])) {
					sql('DELETE FROM debate WHERE id=?', array($_GET['delete']));
					sql('UPDATE `email` SET poll = 0 WHERE token=? AND id_proposal=?', array($_GET['token'], $_GET['id']));
					redirect(ROOT.'?id='.$_GET['id'].'&token='.$_GET['token'].'&check_closing');
				}				
				include 'app/tpl/poll.form.php';
			}
			else {
				infobox('Votre courriel ou votre identifiant unique ne sont pas présents dans la base de donnée. Êtes-vous autorisé à participer ?', 'error');
				include 'app/tpl/forget_link.form.php';
				if(isset($_POST['forget_email'])) {
					$query = $bdd->prepare('SELECT token, id_proposal, email FROM email WHERE id_proposal=? AND email=?');
					$query->execute(array($_GET['id'], is_email($_POST['forget_email'])));
					$data = $query->fetch();
					if(isset($data['email'])) {
						$url = ROOT.'?id='.$data['id_proposal'].'&token='.$data['token'];
						send_mail($data['email'], '[consultation] Demande d’adresse oubliée', 'Bonjour, '.PHP_EOL.' vous, ou quelqu’un d’autre, a demandé le sésame permettant de participer à un débat ! '.$url.PHP_EOL, $addheaders=null);
					}	
				}
				if($is_open == 1) {
					include 'app/tpl/subscribe_proposal.form.php';
					if(isset($_POST['subscribe'])) {
						$email = is_email($_POST['subscribe']);
						$token = sha1(uniqid());
						$subscribe = $bdd->prepare('SELECT email FROM email WHERE id_proposal=? AND email=?');
						$subscribe->execute(array($_GET['id'], $_POST['subscribe']));
						$data_subscribe = $subscribe->fetch();
						if(!isset($data_subscribe['email'])) {
							sql('INSERT INTO `email`(`id_proposal`,`email`,`poll`, `token`) VALUES (?, ?, ?, ?)', array($_GET['id'], $email, 0, $token));
							$url = ROOT.'?id='.$_GET['id'].'&token='.$token;
							send_mail($email, '[consultation] Vous êtes invités à donner votre avis !', 'Bonjour, '.PHP_EOL.'vous êtes invités à donner votre avis à l’adresse suivante : '.$url.PHP_EOL.'Attention, gardez-là précieusement. Elle vous permet d’avoir accès au débat !');
							infobox('Vous êtes inscrit !', 'result');
						}
						else {
							infobox('Vous êtes déjà inscrit !', 'error');
						}
					}	
				}
			}
		}
	}
	else {
		$emails = isset($_GET['emails']) ? base64_url_decode($_GET['emails']) : null;
		include 'app/tpl/create_proposal.form.php';
		if(isset($_POST['save_emails'])) {
			echo '<input type="text" value="'.ROOT.'?emails='.base64_url_encode($_POST['email']).'" style="width:100%"/>';
		}
		else {
			if(isset($_POST['content'])) {
				if(time() >= strtotime($_POST['deadline'])) {
					infobox('La date-butoir doit être plus tard que maintenant !', 'error');
				}
				else {
					$_POST['open'] = (isset($_POST['open'])) ? 1 : 0;
					sql('INSERT INTO `proposal`(`time`,`deadline`,`content`, `open`) VALUES (?, ?, ?,?)', array(time(), strtotime($_POST['deadline']), $_POST['content'], $_POST['open']));
					$lastid = $bdd->lastInsertId();
					$array_email = explode(',', $_POST['email']);
					foreach($array_email as $email) {
						$email = is_email($email);
						$token = sha1(uniqid());
						sql('INSERT INTO `email`(`id_proposal`,`email`,`poll`, `token`) VALUES (?, ?, ?, ?)', array($lastid, $email, 0, $token));
						$url = ROOT.'?id='.$lastid.'&token='.$token;
						send_mail($email, '[consultation] Vous êtes invités à donner votre avis !', 'Bonjour, '.PHP_EOL.'vous êtes invités à donner votre avis à l’adresse suivante : '.$url.PHP_EOL.'Attention, gardez-là précieusement. Elle vous permet d’avoir accès au débat !');
					}
					redirect(ROOT.'?id='.$lastid);
				}
			}
		}
		
	}	
}
include 'app/tpl/footer.php';
?>